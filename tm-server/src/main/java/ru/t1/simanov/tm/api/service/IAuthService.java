package ru.t1.simanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.model.Session;
import ru.t1.simanov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    String login(
            @Nullable String login,
            @Nullable String password
    );

    void logout(@Nullable Session session);

}
